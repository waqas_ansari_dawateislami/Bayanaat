package com.dawateislami.bayanaat.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import com.dawateislami.bayanaat.R;

/**
 * Created by KFMWA916 on 4/28/2017.
 */

public class Utils {

    //***************************************SET AND GET FONT SIZE**********************************
    private static final String FONT_FILE = "font_size";
    public static void saveFontSize(Context context, int font) {
        SharedPreferences.Editor editor = context.getSharedPreferences(FONT_FILE, Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.putInt("font", font);
        editor.putBoolean("is_font_saved", true);
        editor.apply();
    }
    public static int getFontSize(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(FONT_FILE, Context.MODE_PRIVATE);
        return preferences.getInt("font", 0);
    }
    public static boolean isFontSaved(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(FONT_FILE, Context.MODE_PRIVATE);
        return preferences.getBoolean("is_font_saved", false);
    }

    public static void updateSize(TextView textView, Context context) {
        int size;
        if(isFontSaved(context))
            size = getFontSize(context);
        else size = 14;

        textView.setTextSize(size);
    }
    public static void updateSize(WebView webView, Context context) {
        int size;
        if(isFontSaved(context))
            size = getFontSize(context);
        else size = 14;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            Utils.updateFontSizeHigherAPI(webView.getSettings(), size);
        } else Utils.updateFontSizeLowerAPI(webView.getSettings(), size);
    }
    public static void updateSize(WebView webView, int size) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            webView.getSettings().setTextZoom(size);
        } else Utils.updateFontSizeLowerAPI(webView.getSettings(), size);
    }



    //***************************************SETTING FONT SIZE***********************************
    private static void updateFontSizeLowerAPI(WebSettings settings, int progress) {
        int mod = progress % 5;
        if(mod != 0) {
            if(mod >= 3) {
                progress = progress + (5-mod);
            } else {
                progress = progress - mod;
            }
        }


        int size = progress / 5;
        switch (size) {
            case 0:
                settings.setTextSize(WebSettings.TextSize.SMALLEST);
                break;
            case 1:
                settings.setTextSize(WebSettings.TextSize.SMALLER);
                break;
            case 2:
                settings.setTextSize(WebSettings.TextSize.NORMAL);
                break;
            case 3:
                settings.setTextSize(WebSettings.TextSize.LARGER);
                break;
            case 4:
                settings.setTextSize(WebSettings.TextSize.LARGEST);
                break;
        }

    }
    private static void updateFontSizeHigherAPI(WebSettings settings, int progress) {
        //progress: 0 - 25
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            settings.setTextZoom(progress * 10);
        }
    }
    //*****************************************************************************************************
    //*********************************************************************************************************


}
